<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';






function LibNewsletter_upgrade($sVersionBase, $sVersionIni)
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	
	$functionalities = new bab_functionalities();
	
	$addon = bab_getAddonInfosInstance('LibNewsletter');
	
	
	$functionalities->registerClass('Func_Newsletter', $addon->getPhpPath() . '/newsletter.class.php');
	$functionalities->registerClass('Func_Newsletter_Ymlp', $addon->getPhpPath() . '/api/ymlp/ymlp.class.php');
	$functionalities->registerClass('Func_Newsletter_Mailjet', $addon->getPhpPath() . '/api/mailjet/mailjet.class.php');
	$functionalities->register('Ovml/Container/LibNewsletterLists', $addon->getPhpPath().'/ovml.class.php');

	return true;
}


function LibNewsletter_onDeleteAddon()
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	$functionalities = new bab_functionalities();
	$functionalities->unregister('Newsletter/Mailjet');
	$functionalities->unregister('Newsletter/Ymlp');
	$functionalities->unregister('Newsletter');
	$functionalities->unregister('Ovml/Container/LibNewsletterLists');
	return true;
}
