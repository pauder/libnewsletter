<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';



bab_functionality::includeOriginal('Ovml/Container');




/**
 * @return Func_Newsletter
 */
function LibNewsletter_getNewsletter($provider)
{
	if (isset($provider) && false !== $provider)
	{
		$func = bab_functionality::get("Newsletter/$provider");
	} else {
		// use the default functionality
		$func = bab_functionality::get("Newsletter");
	}
	
	if (false === $func)
	{
		throw new Exception('Wrong provider parameter in OVML');
	}
	
	return $func;
}



/**
 * 
 * <OVLibNewsletterLists provider="Mailjet | Ymlp">
 * 
 * 		<OVListId> :	List ID
 *  	<OVListName> :  List name
 * 
 * </OVLibNewsletterLists>
 *
 */
class Func_Ovml_Container_LibNewsletterLists extends Func_Ovml_Container
{
	private $lists;
	
	public function setOvmlContext(babOvTemplate $ctx)
	{
		parent::setOvmlContext($ctx);
	
		$provider = $ctx->get_value('provider');
	
		$nl = LibNewsletter_getNewsletter($provider);
	
		$this->lists = $nl->getLists();
		$this->count = count($this->lists);
		$this->idx = 0;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see utilit/Func_Ovml_Container#getnext()
	 */
	public function getnext()
	{
		if (list($id, $name) = each($this->lists)) {
			$this->ctx->curctx->push('CIndex', $this->idx);
			$this->ctx->curctx->push('ListId', $id);
			$this->ctx->curctx->push('ListName', $name);
			$this->idx++;
			return true;
		}
		
		return false;
	}
}