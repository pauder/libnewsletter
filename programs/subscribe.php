<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/sitemap.php';

// record a subscription from a form with the default functionality

/**
 * 
 * @param string 	$email
 * @param array 	$lists
 * @param string	$next_page	next page URL
 * @throws Exception
 */
function LibNewsletter_addContact($email, $lists = null, $next_page = null)
{

	if (empty($email))
	{
		throw new Exception(LibNewsletter_translate('The email is mandatory'));
	}
	
	if (!preg_match("/^[a-zA-Z0-9+_\.-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}$/", $email))
	{
		throw new Exception(LibNewsletter_translate('This is a wrong email format'));
	}
	
	if (null !== $lists)
	{
		// verifier que c'est bien une liste d'ID
		
		if (!is_array($lists))
		{
			throw new Exception(LibNewsletter_translate('The list of subscriptions must be an array of list ID'));
		}
	}
	
	$NL = bab_functionality::get('Newsletter');
	/*@var $NL Func_Newsletter */
	
	if (null === $lists)
	{
		$lists = array_keys($NL->getLists());
	}
	
	
	$NL->addContact($email, $lists);
	
	// redirect to sitemap item
	
	
	if (null === $next_page)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibNewsletter/');
		if ($id_function = $registry->getValue('next_page'))
		{
			// fin URL from sitemap item
			$next_page = bab_siteMap::rewrittenUrl($id_function);
		}
	}
	
	
	
	if (null !== $next_page)
	{
		$page = new bab_url($next_page);
		$page->location();
	}
	
	
	
	// get back to previous page
	
	if (isset($_SERVER['HTTP_REFERER']))
	{
		$referer = new bab_url($_SERVER['HTTP_REFERER']);
		$referer->location();
	}
	
	
	die('<script type="text/javascript">history.back();</script>');
}


try {
	LibNewsletter_addContact(bab_pp('email'), bab_pp('lists', null), bab_pp('next_page', null));
} catch(Exception $e)
{
	global $babBody;
	$babBody->addError($e->getMessage());
}